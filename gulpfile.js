'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concatCSS = require('gulp-concat-css'),
    concat = require('gulp-concat'),
    minifycss = require('gulp-minify-css'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    notify = require("gulp-notify"),
    uglify = require('gulp-uglify');

var config = {
	scssPath: './app/scss',
	bowerDir: './bower_components'
}

gulp.task('styles', function() {
    gulp.src('app/scss/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
   		.pipe(gulp.dest('app/css/'))
    	.pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
    	.pipe(gulp.dest('app/css/'))
    	.pipe(notify("Gulp create!"));
});
// gulp.task('javascript', function() {
//     gulp.src('app/libs/bootstrap-sass-3.3.6/assets/javascripts/**/*.js')
//         .pipe(uglify())
//         .pipe(gulp.dest('app/js/'))
//         .pipe(notify("Gulp create!"));
// });

/*Bootstrap JS Build==============================================*/
gulp.task('getbootjs', function () {
    gulp.src([
        'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/button.js',
        'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/affix.js',
        //'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/alert.js',
        // 'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/carousel.js',
        'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/collapse.js',
        'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/dropdown.js',
        //'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/modal.js',
        //'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/popover.js',
        //'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/scrollspy.js',
        //'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/tab.js',
        //'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/tooltip.js',
        //'app/libs/bootstrap-sass-3.3.6/assets/javascripts/bootstrap/transition.js',
    ])
    .pipe(concat('bootstrap.js'))
    .pipe(gulp.dest('app/js/'));
});


//Watch task
gulp.task('default',function() {
    gulp.watch('app/scss/**/*.scss',['styles']);
    // gulp.watch('app/libs/**/*.js',['getbootjs']);
});